---
title: "General"
linkTitle: "General"
type: docs
description: >
  Development rules and expectations.
weight: 1
robotsdisallow: true
exclude_search: true
---

# General

## Chain of Command

* Head Admin
	* Release Manager
		* Developers

## Development Rules

* Private code/plugins/configs/etc may not be shared with anyone not part of L2D Development, publically posted, utilized for personal use or gain, or used or shared in any way not explicitly permitted.
* All code regardless of who wrote it belongs to Minecraft Left2Die and may not be used in any way that would violate these rules even if the original author leaves the team.  Code written for any open source projects will still belong to the author, however, Minecraft Left2Die will maintain an irrevocable license to continue use of such code.
* All commits must be PGP signed.
* Gitlab accounts must be configured with 2fa access.
* No one but yourself may have access to your Gitlab or other development accounts.
* Treat all members of the team with respect.

## Expectations

* Developers are not paid. As such, there is no expectation that you have to be available all the time. You do not need to report that you will be gone for a few days. Life > Minecraft.
* If you say you will do something, do it.  If you can no longer commit to it, say something.
* To protect the security of the server, developers that are gone for more than a month without notifying an admin may be demoted and have all access revoked for inactivity.

## Communications

* Discord - All developers need to be in the community Discord.

## Perks

* All developers receive the same perks as the Survivor donation rank.
* Developers that leave the team on good terms will keep the Survivor rank regardless of their donation status prior to becoming a developer. Developers that leave on poor terms will be put on their original rank prior to becoming developer.

