---
title: "Managing Permissions"
linkTitle: "Managing Permissions"
type: docs
description: >
  Steps to manage and maintain server permissions.
robotsdisallow: true
exclude_search: true
---

These steps are purely for maintaining permissions both for new local test builds and in the event that the main server needs rebuilt.  The SQL file stored in the repo for the LuckPerms permissions has no effect on a server after it is originally built.

1. Create permissions in-game as usual.
2. Edit the file `l2d-network/mariadb/sql/global_luckperms.sql`.
  1. Find the exising permissions and go to the bottom of the list.
  2. Replace the semicolon at the end of the last line with a comma.
  3. Create a new line for each new permission.  The last one should end with a semicolon and not a comma.
3. Commit and merge the change.

The following is how each permission line is formatted:

```
(NULL,'<GROUP>','<PERMISSION>',<0 for disabled, 1 for enabled>,'<SERVER>','<WORLD>',0,'{<CONTEXTS>}'),
```

