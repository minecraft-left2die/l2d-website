---
title: "Update Themed Map Regions"
linkTitle: "Update Themed Regions"
type: docs
description: >
  Steps to add, remove, or update the WorldGuard regions in the Themed world.
robotsdisallow: true
exclude_search: true
---

Due to the way that the maprotation of the Themed maps works, WorldGuard regions can't simply be edited as normal.  If these steps aren't followed, any changes will be undone when the server restarts.

1. Make changes via the `wg` command as usual.
2. In the `l2d-network/configs/l2d/scripts/maprotate/region` directory, execute the `./gen-patch.sh` script.
3. Commit the changes made to the files in that directory and **not** the standard WorldGuard directory for the Themed world.
4. Follow standard change control to get the change in the `master` branch.`

