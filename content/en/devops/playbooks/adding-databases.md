---
title: "Adding Databases"
linkTitle: "Adding Databases"
type: docs
description: >
  Steps to add a new MySQL database for a plugin.
robotsdisallow: true
exclude_search: true
---

## Creating a new Database

1. Go to `l2d-network/mariadb/migrations`
2. Create a new version file for the new database.  It should follow the format of `V#__<SERVER>_<PLUGIN>.sql`.
  - The `#` should be one higher than the current highest number of the files in the list.
  - If the database will be used on more than one server, use the keyword `global` for the `<SERVER>`.
  - The double underscores after the `V#` are required.  **It cannot be one underscore.**
3. Add the SQL statements in the file required for creating the database and giving it it's base permissions.
  - The name of the database should be the same as the `<SERVER>-<PLUGIN>` format used in the file name.
  - The database user should be the same user as the rest of the environment.

```
CREATE DATABASE IF NOT EXISTS <DATABASE-NAME>;
GRANT ALL ON `<DATABASE-NAME>`@`*` TO `<DATABASE-USER>`@`%`;
```

4. If this plugin will have any configurations stored in the database, these should also be included in the file.  See `Getting Config Data from a Database` below.
5. **IMPORTANT: Once this file is deployed to the production server, it must never be changed again!**  If any changes are needed in the future, a new version file will need created.
6. Add the new database to the `.secrets.dev` and `.secrets.prod` files.  The database names are different in prod.  Follow the convention used by the other databases in the file.
7. Add the secrets files and the new database versioning file to git (`git add`).  Do not commit yet.
8. Verify with `git diff --cached` that the keys you used in the secrets files are being properly placed in the new SQL files.
9. Commit the changes and follow proper change control procedures with a merge request.
10. The database migrations are run and verified every time *any* server is restarted.


## Getting Config Data from a Database

This is only necessary if the plugin contains configuration data that differs from defaults and is stored in the database.

1. Create the migration file up to step 3 above.
2. Follow the procedure for adding the new plugin and start the servers.
3. Configure the plugin as necessary.
4. Connect to the MySQL console and see which tables will need to be included in the migration script.

```
./scripts/mysql.sh console
> USE <DATABASE-NAME>;
> SHOW TABLES;
# To see what is in a table:
> SELECT * FROM <TABLE-NAME>;
> exit
```

5. Perform a dump of the tables needed:

```
./scripts/mysql.sh <DATABASE> <TABLES>
```

6. The `CREATE TABLE` and any `INSERT` statements should be added to the migration file for the specified database.
7. The `id` fields (name may vary slightly depending on the database) should be replaced in any insert statements from the existing number to `NULL`.


