---
title: "Upgrading the Server"
linkTitle: "Server Upgrade"
type: docs
description: >
  Steps to upgrade the server to a newer Minecraft version.
robotsdisallow: true
exclude_search: true
---

**This process must be followed for each Minecraft version.  The server cannot skip upgrading to a version.  Doing so increases the risk of world corruption.**

The server is typically upgraded in stages: Proxy --> Lobby --> Buildmaps --> L2D.  Muliple components can be done together to have less upgrade stages, but must stick to this order over all.  In other words this is valid: Stage 1: Proxy and Lobby, Stage 2: Buildmaps, Stage 3: L2D.  However, this order should not be done: Stage 1: Lobby, Buildmaps, Stage 2: Proxy, L2D (Proxy must be in the first stage!).

Every component could be upgraded all at once in a single stage, just keep in mind, upgrading is easy; downgrading is hard (backup restores).

## docker-compose.yml

For each server, depending on which upgrade stage you're working on, update the version and build variables:

- Velocity
	- VELOCITY_VERSION
	- VELOCITY_BUILD
- Purpur (lobby, buildmaps, l2d)
	- PURPUR_VERSION
	- PURPUR_BUILD

The respective values for these variables can be found on their download pages:

- [Velocity](https://papermc.io/downloads/velocity)
- [Purpur](https://purpurmc.org/downloads)

The number after the `#` is the build number.  The next time the container starts/restarts, it will automatically download the specified server jar file.

## Plugins

In the `plugins` directory, under each server is a `plugins.conf` file.  Each server will need the variables for each plugin updated to point to the latest version supported by the Minecraft version to be upgraded to.

There are four types of variables that can potentially be defined:

- **PLUGIN_URLS** - URLs that will directly download the plugin.  Typically found on the plugins Github/Gitlab releases page.
	- Plugins can also be directly downloaded from Bukkit  You must first download the plugin yourself, then in your browser's download manager, right click and copy the download URL.
- **PLUGIN_SPIGET_IDS** - IDs for the plugins downloaded from [SpigotMC](https://www.spigotmc.org/) via the [Spiget API](https://spiget.org/)
	- The ID is the number after the period at the end of a Spigot plugin URL.
	- This does not work well for Spigot plugins that redirect to a third party.
- **MANUAL_PLUGIN_URLS** - URLs to plugins that need to be downloaded manually if neither of the above will work.
- **MANUAL_COMPILE_PLUGINS** - List of plugins that must be compiled from source manually.

Executing the `scripts/plugin-download.sh` script will download the plugins defined in the `plugin.conf` files.

## Testing

Execute `docker-compose up --detach` (assuming it's not already running) to start up the servers.

- Everything
  - Verify that login is successful
  - Verify that all plugins loaded without error
- Lobby
	- There aren't many chunks on the map.  Any corrupted chunks should be immediently apparent.
- Buildmaps
	- Fly around every map and verify that no chunks are corrupted.
		- For any chunk that is corrupted, the best bet is to create a schematic on the un-upgraded prod build server and attempt to paste that on the upgraded test build server.  This is not guaranteed to work.
		- A re-try of the upgrade will generally not work.
		- As last resort, any corrupted chunks will either need rebuilt manually or retire that map.
- L2D
	- Verify the redstone of puzzles work properly
	- Play the game a bit as a normal user, run various commands, make sure things seem to be working as expected.
	
### Committing

- Commit map changes to their respective repos.
- Commit changes to each server's respective repo.  If there's a submodule for a map, make sure that the pointer file is also added to the commit.  Push to a branch, and create a merge request.
- Commit changes to `docker-compose.yml`, `plugins.conf`, and the indicidual servers' submodule objects.  Push to a branch and create a merge request.
- Once the changes are merged to `master`, they will be deployed.
	- The changes won't become effective until the next restart of the respective server.


