---
title: "Development Environment"
linkTitle: "Development Environment"
type: docs
description: >
  Instructions for setting up the local development environment.
robotsdisallow: true
exclude_search: true
---

## Required Software
### Required for All Roles

* **[Git](https://git-scm.com/)** - The version control system that all L2D source code is managed with.
* **[GitLab Account](https://gitlab.com/)** - The remote git repo storage provider of the L2D code repositories.
* **[Trello Account](https://trello.com/)** - The Kanban board used for organizing development task cards.
* **[GPG](https://gnupg.org/)** - Used for signing Git commits, unlocking encrypted files in repos, and for encrypted email communication.
* **Two Factor Authentication App** - Used for accessing Gitlab with 2fa.  Recommend [Aegis Authenticator](https://play.google.com/store/apps/details?id=com.beemdevelopment.aegis&hl=en_US&gl=US) (Android) or [Tofu](https://apps.apple.com/app/tofu-authenticator/id1082229305) (iPhone).
* **[Pre-Commit](https://pre-commit.com/)** - Performs sanity checks on the repositories prior to making commits.
* **[Podman](https://podman.io/)** - Used to run the server containers.
* **[Docker Compose](https://docs.docker.com/compose/)** - Used to manage server containers.

### Required for DevOps Role

* **[Ansible](https://www.ansible.com/)** - Used for deployments to the main L2D server.
* **[git-crypt](https://github.com/AGWA/git-crypt)** - Used for encrypting production secret values in the repositories.  Only required for those with access to these files.
* **[git-lfs](https://git-lfs.com/)** - Used for repos containing the server worlds.
* **[Terraform](https://www.terraform.io/)** - Used for deployment of infrastructure on third party services.
* **[Terragrunt](https://terragrunt.gruntwork.io/)** - A wrapper around Terraform.
* **[tflint](https://github.com/terraform-linters/tflint)** - Used to verify Terraform configurations.
* **[tfsec](https://github.com/aquasecurity/tfsec)** - Used to verify Terraform security.
* **[terraform-docs](https://github.com/terraform-docs/terraform-docs/)** - Used to generate Terraform docs.
* **[Shellcheck](https://www.shellcheck.net/)** - Used by pre-commit for some checks.

### Required for Java Developer Role

* **Java 17** - The version of Java that the L2D code base is compiled against.  This can either be Oracle Java 17 or OpenJDK 17.
* **[IntelliJ Idea](https://www.jetbrains.com/idea/)** - Java IDE.  Other IDEs can be used so long as they don't interfere with the formatting and line endings of code written with IntelliJ.
* **[Maven](https://maven.apache.org/)** - Used for the compilation of most of the L2D code base.
* **[Gradle](https://gradle.org/)** - Used for the compilation of some third party forked plugins.  Only needed if working on those particular plugins.

**Windows Users:** Editing code must be done with software that supports Linux line endings such as Intellij.

### Recommended Software

* **[SmartGit](https://www.syntevo.com/smartgit/)** - Graphical Git interface for those that prefer the GUI over the command line.

## Environment Setup

### Gitlab

1. Go to [https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in) and create a GitLab account if you don't already have one.  Please choose a secure password.  Note that the name and email you use will be public in your git commits.  Choose appropriately.
2. Install an authenticator app to your phone if you don't already have one that support the Google Authenticator protocol.  You can either install the one recommended in the section above for your phone OS or any other authenticator app that supports the Google Authenticator protocol.  **You will be unable to see the repos until this is setup!**
3. In GitLab, go to User Settings (top right) --> Account --> Manage Two-Factor Authentication.  Follow the instructions for app based authentication using the authenticator app on your phone.
4. You'll need to provide your GitLab username to an admin and wait for access to be setup before continuing.

### Windows

1. Open the Start Menu and search for “features”.  Select “Turn Windows Features on or off”.
2. Check the box for “Windows Subsystem for Linux” and click “OK”.  Restart the system when prompted.
3. Open the Start Menu and search for “Store”.  Select “Microsoft Store”.
4. Search “Linux” in the search box.  Select “Ubuntu” (if experienced with other Linux distros, you can select  a different option, however this guide will be focused on Ubuntu).
6. Click “Install”.
7. Once installed, click “Launch”.  It will then continue with the installation process.
8. When prompted, create a username and password.  Note that no characters will appear while typing your password.
9. Open Ubuntu from the Start Menu.
10. When all is finished, continue with the Linux instructions below.

### MacOS

The latest MacOS systems on the M1 chips or higher are not directly compatible with L2D's Dockerized setup.  It is possible, but out of scope for this document.

### Linux

#### Install Required Software

##### Required by All

***Note that this process has not been tested on Windows Subsystem for Linux (WSL).  The podman parts may not work and need tweaking.***

Install the base packages:

`apt install -y git gpg pre-commit podman podman-docker`

Configure the Podman socket:

```
systemctl --user enable podman.socket
systemctl --user start podman.socket
systemctl --user status podman.socket
```

Configure the podman socket:

```
echo "export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock" >> ~/."$(echo $SHELL | awk -F/ '{print $NF}')"rc
source ~/."$(echo $SHELL | awk -F/ '{print $NF}')"rc
```

Configure access to the Docker registry:

```
sudo cat << EOF >> /etc/containers/registries.conf
[registries.search]
registries = ['docker.io']
EOF
```

Download Docker Compose:

*Note: as of Ubuntu 22.04, the `docker-compose` package in the repositories does not work for this setup)*

`sudo curl -SL https://github.com/docker/compose/releases/download/v2.15.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose`

Create a Gitlab Access Token:

- In your Gitlab account, go to your user icon --> Preference --> Access Tokens.  Click "Add New Token".
- Give it a name like "Podman Login".
- Set the scope to "read_registry".  
- Click "Create personal access token"
- Copy the token and use it as the password in the next step.  You will not be able to copy the token once leaving the page.

Authenticate with Gitlab Container Registry:

`podman login --authfile ~/.docker/config.json registry.gitlab.com`

User your Gitlab username and the Access Token from the previous step as the password.


##### Required by Devops

`apt install ansible git-crypt terraform shellcheck`

Install `terragrunt`, `tflint`, `tfsec`, and `terraform-docs` following the directions on each of their respective sites listed near the top of this page.

##### Required by Java Developers

`apt install openjdk-17-jdk openjdk-17-jre maven gradle`

Install IntelliJ or preferred IDE:

- **For Ubuntu:** `sudo snap install intellij-idea-community`
- **For Windows:** See link near top of this page.


#### Create Keys

##### SSH Key Pair

If you don't already have an SSH key pair, create one with:

	ssh-keygen -t ed25519

Select the default path and set a passphrase (this can be left blank).  Now print out the public key:

	cat ~/.ssh/id_ed25519.pub

Return to your GitLab account.  Go to User Settings --> SSH Keys.  Paste your public key, give it a title and click "Add key".

##### GPG Key Pair

If you don't already have a GPG key pair, create one with:

	gpg --gen-key

Specify your name as you want it to appear on the key, the email associated with your GitLab account, and a secure passphrase (this one will be used a lot for git commits).  Now print your public key with:

	gpg --export -a <your-email> > public.key
        cat public.key

Return to your GitLab account.  Go to User Settings --> GPG Keys.  Paste your public key and click "Add key".  Also send the `public.key` file to an existing repo admin.


#### Configure Git

First, get your gpg key ID:

`gpg --list-keys`

Find the key associated with your email address and select the long strind of letters/digits.  This is the `GPG-KEY-ID` used below.

***These commands configure git globally.  If you already have a git setup that differs from the one you're using for L2D, make changes to how you configure git accordingly.***

```
git config --global user.name <YOUR-GIT-NAME/USERNAME>
git config --global user.email <YOUR-GIT-EMAIL>
git config --global user.signingkey <GPG-KEY-ID>
git config --global gpg.program /usr/bin/gpg2
git config --global commit.gpgsign true
```

This is the bare minimum.  Customize to your liking.

#### Clone the Repos

1. Creating a working directory and change to it
```
	mkdir ~/workspace
	cd ~/workspace
```
2. Clone the repos you'll need for whatever you'll be working on.  At minimum, the `l2d-network` must be cloned.
3. Inside the `l2d-network` repo, execute the `./scripts/setup.sh` script.

