---
title: "Terraform"
linkTitle: "Terraform"
type: docs
description: >
  Information on the Terraform setup for L2D for third-party service management.
robotsdisallow: true
exclude_search: true
---

Terraform is used to managing third party tools used by L2D, in particular Cloudflare and Gitlab.

## Setup

### Prerequisites

Install the following software packages:

```
sudo apt install git git-crypt terraform pre-commit
```

In addition, the following need installed manually.  Follow the instructions on their respective pages.

- [terragrunt](https://terragrunt.gruntwork.io/docs/getting-started/install/)
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/aquasecurity/tfsec)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs/)

### Repo Setup

Your PGP key will need added by a repo admin before continuing.

```
git clone git@gitlab.com:minecraft-left2die/infrastructure/terraform-l2d.git
pre-commit install
git crypt unlock
```

### Repo Structure

The repo has the following structure:

- Base
  - deployments - Contains the base Terragrunt config
    - l2d - Contains each Terragrunt module for each service
  - modules
    - stacks - Contains the Terraform modules called upon by Terragrunt.

### Pipeline

A Gitlab CICD pipeline manages deployments of any changes to the Terraform code.  It consists of two stages:

- Test
  - Performs a `tflint` and a `terragrunt plan` against a merge request.  The output should be verified prior to merging to the master branch to ensure it is what is expected.
- Deploy
  - Performs a `terragrunt apply` against the third party services deploying any changes.
  - Commits any changes to the Terraform state files and push back to the repo.



