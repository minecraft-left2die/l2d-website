---
title: "Gitlab"
linkTitle: "Gitlab"
type: docs
description: >
  Information on Gitlab where all code is stored.
robotsdisallow: true
exclude_search: true
---

All of L2D's code is stored in Gitlab.  Create an account at [gitlab.com](https://gitlab.com/) and send your username to a current admin to be added to repos.  **You must have 2FA enabled on your account to be able to access any repos!**

Keep in mind that the name and email address you use will be public in git commits and on your profile.

## Enabling 2FA

1. Click your user icon in the top left.
2. Click "Preferences".
3. Click "Account"
4. Click "Manage two-factor authentication"
5. Setup 2FA using your app of choice.

