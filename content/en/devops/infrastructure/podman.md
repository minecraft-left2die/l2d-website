---
title: "Podman"
linkTitle: "Podman"
type: docs
description: >
  Information on Podman used for Docker container management.
robotsdisallow: true
exclude_search: true
---

## Installation

### Add the GPG Key for the Podman Repository

```curl -s https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/Release.key | gpg --dearmor | sudo tee /usr/share/keyrings/libcontainers.gpg > /dev/null```


### Add the Podman Repository

```
cat << EOF | sudo tee /etc/apt/sources.list.d/libcontainers.sources > /dev/null
Types: deb
URIs: https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/
Suites: /
Signed-By: /usr/share/keyrings/libcontainers.gpg
EOF
```

### Install Podman

```
apt update
apt install podman-rootless podman-plugins podman-machine-cni uidmap containernetworking-plugins python3-dotenv slirp4netns
```

### This needs added to clear up an error when running containers under a non-root user

```
cat << EOF | sudo tee /etc/containers/containers.conf > /dev/null
[engine]
cgroup_manager = "cgroupfs"
events_logger = "file"
EOF
```

### Enable unprivileged_userns_clone kernel module to allow non-root user to start containers

```sysctl -w kernel.unprivileged_userns_clone=1```


### Non-Root Container User

Create the user that will run `podman` at the specifed path:

```useradd -d /<HOME-PATH>/<USER> <USER>```


### Prevent containers from stopping when logging out of the non-root user account

Specify the UID of the user that will be running `podman`:

```loginctl enable-linger <UID>```


### Start the Podman Socket

These should be run as the user that will run `podman`:

```
systemctl --user enable podman.socket
systemctl --user unmask podman.socket
systemctl --user start podman.socket
```
