---
title: "AnnouncerPlus"
linkTitle: "AnnouncerPlus"
type: docs
description: >
  Plugin for in server announcements.
robotsdisallow: true
exclude_search: true
---

Announcer Plus displays the in game announcements that appear every few minutes.  It also displays the large visual warnings that the server is about to restart (not the chat ones).

## Configuration

### Announcements

All annoucements are configured in the `message-configs` directory.  In order for the messages to appear for a player, a permission needs to be set for the group they're in.  The permission node follows the format of `announcerplus.messages.<FILE-NAME>`.  ie.  `announcerplus.messages.plotworld` for the messages in `plotworld.conf`.

### Disabled Features

These features are disabled as they conflict with the L2D core plugin:

```
first-join-config-enabled=false
join-features=false
quit-features=false
```

As a result, the `first-join.conf` config file and the contents of the `join-quit-configs` directory have no use for L2D.

