---
title: "nuVotifier"
linkTitle: "nuVotifier"
type: docs
description: >
  The vote receiver plugin.
robotsdisallow: true
exclude_search: true
---

nuVotifier is used for receiving votes from the voting sites and passing it to other plugins that handle awards, etc.

## Configuration

### Proxy

#### config.toml

On the proxy (Velocity), the configs are located in `plugins/nuvotifier`.  The following configuration is needed to allow the votes to pass from the proxy to L2D:

- **port:** - the port that vote sites will communicate with.  This must be setup in each voting site's settings page.
- **tokens.default:** The default token is not being used.  See `forwarding.proxy.Hub`.
- **forwarding.method:** This must be set to `proxy` so that nuVotifier proxies the votes from the proxy server to L2D.
- **forwarding.proxy.Hub.address:** The IP address for L2D's server.
- **forwarding.proxy.Hub.port:** The nuVotifier port defined on the L2D server's copy of nuVotifier.
- **forwarding.proxy.Hub.token:** The private token to communicate with the L2D server with.  This token will need added to the L2D nuVotifier config as well.

If any other servers were added to the network that needed to receive votes, a new `forwarding.proxy` section would need created for that server.

#### RSA Keys

In the `rsa` directory is a public and private key.  The public key will need to be provided in the voting site settings.  This allows the voting sites to authenticate with L2D's nuVotifier proxy.

### L2D

On L2D (or any other non-proxy Minecraft server), the configs are located in `plugins/Votifier`.  The RSA keys generated here are not needed.

#### config.yml

The following configuration is needed for L2D:

- **host:** The IP address for L2D (should match `forwarding.proxy.Hub.address` from the proxy).
- **port:** The nuVotifier port for L2D (should match `forwarding.proxy.Hub.port` from the proxy).
- **tokens.default:** The private token for the L2D server (should match `forwarding.proxy.Hub.token` from the proxy).

