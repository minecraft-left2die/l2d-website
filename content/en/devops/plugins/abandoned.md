---
title: "Abandoned Plugins"
linkTitle: "Abandoned Plugins"
type: docs
weight: 0
description: >
  A list of abandoned plugins.
robotsdisallow: true
exclude_search: true
---

The following plugins have been abandoned by their owners  They currently still work, but may have broken functionality and may completely break in the future.

- ChestCommands
- Craftconomy3
- FishinRoulette
- Holographic Displays
- PhatLoots
- WDLCompanion

