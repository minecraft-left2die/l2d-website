---
title: "HolographicDisplays"
linkTitle: "HolographicDisplays"
type: docs
description: >
  Plugin for in the holograms in Plotworld.
robotsdisallow: true
exclude_search: true
---

## Configuration

Hologram configs are stored in `database.yml`.  Because the plugin does not allow for separate config files, the Themed map rotation causes this file to change frequently when changing the current Themed map name.  There is no ill effect of this other than `git` constantly showing a modified file.

