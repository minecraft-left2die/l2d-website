---
title: "Wiki Edits"
linkTitle: "Wiki Edits"
type: docs
description: >
  Help with editing and maintaining this wiki
---

This website and wiki are hosted in a public git repository.  There are two ways you can help contribute to this wiki: making suggestions for edits and making edits yourself.

## Suggesting Edits

You can either suggest changes on our [Discord](https://discord.gg/JWZXt8V) or by making a ticket on our [Gitlab](https://gitlab.com/minecraft-left2die/l2d-website) page by clicking the "Create documentation issue" link on the right hand side on any wiki page.  The latter is preferred to avoid suggestions being forgotten or missed.

## Making Edits

To make edits, you will need a [Gitlab](https://gitlab.com) account.  The below will list steps for those without experience using git. If you have git experience, feel free to use whatever git tools you are used too.

{{% alert title="Warning" color="warning" %}}
The name and email you use to create a Gitlab account will be public when making a change to this wiki.
{{% /alert %}}

{{% alert title="Tip" color="info" %}}
The wiki pages are written in Markdown format.  View this page on [Markdown](https://www.markdownguide.org/cheat-sheet/) if you aren't familiar with it.
{{% /alert %}}

1. On the right hand side of the page you wish to edit, click the "Edit this page" link.
2. If you aren't already logged into Gitlab, you'll be prompted to do that.
3. Click the down arrow button next to the blue "Open in Web IDE" button (Don't click the IDE button).  Choose "Edit" from the drop down, then click the button that has now changed to say "Edit".  This step won't be needed after you successsfully make your first edit.
4. If you haven't made an edit before or otherwise forked the repository, you will be prompted to fork it.  Click the "Fork" button.
5. Once the fork completes, you'll be shown an editor to make your change.
6. When finished with your change, enter a message in the "Commit message" box  giving a brief description of what you changed.  Then click the "Commit changes" button.
7. Follow the prompts to create a merge request.
8. Once the merge request is approved, it'll appear on the wiki.


The above only covers basic edits of existing pages.  If you want to contribute more advanced edits such as new pages, formatting, etc you are welcome to do so.



