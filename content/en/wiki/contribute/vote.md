---
title: "Voting"
linkTitle: "Vote"
type: docs
description: >
  Voting helps us get new players
menu:
  main:
    weight: 25
---

Voting is an important part of helping the Minecraft Left2Die server grow and gain new players.

{{% alert title="Tip" color="info" %}}
Use the /vote command in game for quick access to voting links and to see how soon you can vote again.
{{% /alert %}}

## Voting Sites

- [Minecraft-MP](http://minecraft-mp.com/server/134038/vote/)
- [Minecraft List](https://minecraftlist.org/vote/3878)
