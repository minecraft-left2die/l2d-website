---
title: "Bosses"
linkTitle: "Bosses"
type: docs
description: >
  Boss fights
---

## Skills

Each boss has a series of skills which are not listed on this page. You will have to fight them to find out what each boss does. The bosses protect themselves from various environmental threats. Therefore, suffocation, drowning, lava, etc will not harm a boss (though they will still appear to be taking damage).

## Bosses

There are several bosses that autospawn within the [maps]({{< ref "/wiki/gameplay/worlds" >}}). The ones that autospawn will respawn one hour after they were last killed regardless of the [restarts]({{< ref "/wiki/gameplay/restart" >}}). The bosses that do not auto spawn are occasionally spawned for mob killing events at random. The [map overviewer](https://map.minecraftleft2die.com/) shows the location of each boss (`/map`).

Most bosses will despawn if all players leave the area or die.

Below is each boss currently in use on Minecraft Left2Die. Bosses will drop other items along with the [uniques](/wiki/gameplay/activities/uniques). You will have to kill them to find out what else they drop.

The bosses below are ordered roughly from easiest to hardest. A boss with lower HP is not necessarily easier than one with higher HP.

### Fluffy

| Mob         | Enderdragon     |
|:-----------:| --------------- |
| Location    | Not Autospawned |
| HP          | 10              |
| Armour      | N/A             |
| Unique Drop | N/A             |

### Zombie King

| Mob         | Zombie              |
|:-----------:| ------------------- |
| Location    | [City]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap/city" >}}) |
| HP          | 1,400               |
| Armour      | Prot IV Diamond Set |
| Unique Drop | Power Sword         |

### Jose the Happy Ghast

| Mob         | Ghast           |
|:-----------:| --------------- |
| Location    | [Central Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP          | 2,000           |
| Armour      | N/A             |
| Unique Drop | Ghast Hoe       |

### Kracken

| Mob         | Squid           |
|:-----------:| --------------- |
| Location    | [Aquashock]({{< ref "/wiki/gameplay/worlds/zombieworlds/themed/aquashock" >}}) |
| HP          | 4,000           |
| Armour      | N/A             |
| Unique Drop | Squiddy Ink     |

### KingSkele

| Mob          | Skeleton          |
|:------------:| ----------------- |
| Location     | [Northwest Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP           | 10,000            |
| Armour       | N/A               |
| Unique Drops | Zim's Bow<br>Crown of Skele |

### Ted the Giant

| Mob         | Giant               |
|:-----------:| ------------------- |
| Location    | [Tiny]({{< ref "/wiki/gameplay/worlds/zombieworlds/themed/tiny" >}}) |
| HP          | 2,000               |
| Armour      | Prot IV Diamond Set |
| Unique Drop | Ted's Tear          |

### Lupen the Demon Wolf

| Mob         | Wolf            |
|:-----------:| --------------- |
| Location    | [South Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP          | 10,000          |
| Armour      | N/A             |
| Unique Drop | Demon Sword<br>The Teeth of Lupen<br>Lupens Chew Toy |

### Sir Clucksalot

| Mob         | Chicken           |
|:-----------:| ----------------- |
| Location    | [Northeast Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP          | 6,000             |
| Armour      | N/A               |
| Unique Drop | Sword of Chickney |

### Bacon

| Mob          | Pig Zombie          |
|:------------:| ------------------- |
| Location     | [SouthWest Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP           | 2,000               |
| Armour       | Prot IV Diamond Set |
| Unique Drops | Bacon Sword<br>Bacon Bit |

### Sheep King

| Mob          | Sheep             |
|:------------:| ----------------- |
| Location     | [SouthEast Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP           | 12,000            |
| Armour       | N/A               |
| Unique Drops | Sheep Fuzz<br>Sheep Axe |

### Zim

| Mob         | Wither              |
|:-----------:| ------------------- |
| Location    | [Aquashock]({{< ref "/wiki/gameplay/worlds/zombieworlds/themed/aquashock" >}}) |
| HP          | 8,000               |
| Armour      | Prot IV Diamond Set |
| Unique Drop | Lemon               |

### Zenderman

| Mob         | Enderman        |
|:-----------:| --------------- |
| Location    | [Zender]({{< ref "/wiki/gameplay/worlds/zombieworlds/themed/zender" >}}) |
| HP          | 2,000           |
| Armour      | N/A             |
| Unique Drop | Pageless Book   |

### Arachnia

| Mob          | Spider            |
|:------------:| ----------------- |
| Location     | [Northwest Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP           | 6,000             |
| Armour       | N/A               |
| Unique Drops | Eye of the Spider Queen<br>Sword of the Arthropods |

### Zerg

| Mob          | Creeper         |
|:------------:| --------------- |
| Location     | [Town]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap/town" >}}) |
| HP           | 6,000           |
| Armour       | N/A             |
| Unique Drops | Zerg Head<br>Zerg Powder |

### The Dark Knight

| Mob         | WitherSkeleton         |
|:-----------:| ---------------------- |
| Location    | [East Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP          | 100,000                |
| Armour      | N/A                    |
| Unique Drop | The Wrath of Ten Hells |

### Bappo

| Mob          | Vex               |
|:------------:| ----------------- |
| Location     | [Southwest Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) |
| HP           | 2000              |
| Armour       | N/A               |
| Unique Drops | Evil Fairy Dust<br>Kitty Claw |

### Inspector #9

| Mob         | Mystery         |
|:-----------:| --------------- |
| Location    | Mystery         |
| HP          | 10,000          |
| Armour      | Mystery         |
| Unique Drop | Inspected by #9 |

## Retired Bosses

### Draco

| Mob         | Enderdragon     |
|:-----------:| --------------- |
| Location    | [City]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap/city" >}}) |
| HP          | 2,000           |
| Armour      | N/A             |
| Unique Drop | Draco Diamond   |

### Killer Rabbit of Caerbannog

| Mob         | Rabbit          |
|:-----------:| --------------- |
| Location    | Olympus         |
| HP          | 10,000          |
| Armour      | N/A             |
| Unique Drop | Xbow            |
