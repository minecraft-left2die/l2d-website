---
title: "Puzzles"
linkTitle: "Puzzles"
type: docs
description: >
  Locate and solve puzzles and mazes
---

Left2Die has several puzzles, parkour, and mazes hidden around the maps with prizes at the end. Below are several of these puzzles sorted roughly in order of easiest to hardest. Some may include hints.

## Active Puzzles

### Islands Maze 1
<figure>
  <img src="/img/wiki/Pvpmaze1.png"  width="278px" height="166px" />
</figure>

**Location:** Islands<br>
**Type:** Maze

- Tiny maze located underground in the Islands map.


### Islands Maze 2

<figure>
  <img src="/img/wiki/Pvpmaze2.png"  width="278px" height="166px" />
</figure>

**Location:** Islands<br>
**Type:** Maze

- Medium sized maze in the Islands map.
- Located completely underwater.


### Islands Maze 3

<figure>
  <img src="/img/wiki/Pvpmaze3.png"  width="278px" height="166px" />
</figure>

**Location:** Islands<br>
**Type:** Maze

- Large sized maze in the Islands map.
- Hidden underground.


### Online Forensics Puzzle

**Location:** Megamap<br>
**Type:** Puzzle

- Solve the puzzle located on [this page](/html/puzzle.html)  to get the coordinates of the unique.


### Physics Homework Puzzle

**Location:** Town, City, Megamap<br>
**Type:** Puzzle

- Consists of two separate puzzles. Started by finding one of the two homework books, one of which is a unique.
- Solve the physics problems to find the hidden uniques.


### Tiny Puzzle

<figure>
  <img src="/img/wiki/Tinypuzzle.png"  width="278px" height="166px" />
</figure>

**Location:** Tiny Terrors<br>
**Type:** Maze, Puzzle

- A secret entrance leads to this maze and puzzle course.


### Parking Lot Puzzle

<figure>
  <img src="/img/wiki/Parkinglot.png"  width="278px" height="166px" />
</figure>

**Location:** City<br>
**Type:** Parkour, Puzzle

- Named for the location in which the entrance was once located.
- Contains five rooms of puzzle and/or parkour.


### Observatory Puzzle

<figure>
  <img src="/img/wiki/Observatory.png"  width="278px" height="166px" />
</figure>

**Location:** North Megamap<br>
**Type:** Puzzle

- It is hiding something.
- This puzzle is easier than many people make it out to be.


### Volcano Maze

<figure>
  <img src="/img/wiki/Volcano.png"  width="278px" height="166px" />
</figure>

**Location:** City<br>
**Type:** Maze

- Finding it is probably the toughest part.
- The maze is entirely in lava.


### Spider Room Puzzle

<figure>
  <img src="/img/wiki/Spiderroom.png"  width="278px" height="166px" />
</figure>

**Location:** City<br>
**Type:** Puzzle

- Gets its name from the loot chamber.
- Hidden somewhere within the sewers of the city.
- Contains two rooms.
- Using enderpearls is a good way to get killed.


### Jose the Happy Ghast's Seed Vault

<figure>
  <img src="/img/wiki/Joses_seed_vault.png"  width="278px" height="166px" />
</figure>

**Location:** Nether<br>
**Type:** Puzzle

- No external information is needed to enter the vault.
- Some parts will require some guess work.
- Vault is highly lethal. You've been warned!


### Enderpearl Puzzle

<figure>
  <img src="/img/wiki/Enderpearl_puzzle.png"  width="278px" height="166px" />
</figure>

**Location:** Tiny Terrors<br>
**Type:** Maze

- Located on the second floor of Tiny Terrors.
- Use enderpearls to navigate the narrow passages through the maze.


### Fly Puzzle

<figure>
  <img src="/img/wiki/Fly_puzzle.png"  width="278px" height="166px" />
</figure>

**Location:** Tiny Terrors<br>
**Type:** Puzzle

- Located on the second floor of Tiny Terrors.
- Find the correct way to open the door. Choose incorrectly and the puzzle is broken for the [restart]({{< ref "/wiki/gameplay/restart" >}}).


### Tnt's Puzzle Arena

<figure>
  <img src="/img/wiki/Tnts_puzzle_arena.png"  width="278px" height="166px" />
</figure>

**Location:** Tiny Terrors<br>
**Type:** Puzzle, Parkour, Maze

- Named after the builder that created it.
- A series of several puzzles, parkour, and maze rooms.
- The clue for it's location is found somewhere in the attic.


### Bedrock 10-Layer Maze

<figure>
  <img src="/img/wiki/Bedrock10layer.png"  width="278px" height="166px" />
</figure>

**Location:** Northeast Megamap<br>
**Type:** Maze

- Located somewhere underground. There is a visible entrance to it.
- There is only one entrance to the maze.
- The top most layers contain spawners.


### Pyramid Puzzle

<figure>
  <img src="/img/wiki/Pyramid.png"  width="278px" height="166px" />
</figure>

**Location:** Northwest Megamap<br>
**Type:** Puzzle, Maze, Parkour

- There are three encoded books and a decoder book hidden elsewhere in the megamap. You will need these to beat the pyramid.
- It cannot be defeated alone.
- Contains five rooms.


### Tower Puzzle

<figure>
  <img src="/img/wiki/Tower.png"  width="278px" height="166px" />
</figure>

**Location:** Central Megamap<br>
**Type:** Parkour, Puzzle, Maze

- There is a "Floor 1" encoded book hidden elsewhere in the megamap. It uses a different code than the Pyramid puzzle. There is no decoder book (does not mean there are not other clues on how to decode it).
- You will need to do some Googling and try and guess the correct cipher. There are no clues anywhere on which cipher is used. It is not a really obscure cipher and should not be difficult to guess.
- Once decrypted, follow the instructions to gain access to the tower.
- You only get one shot per room, per restart period. If you make a mistake, the entire tower will disable itself. Depending on when you make a mistake, you may have to start over at Room 1.



### Megamaze

<figure>
  <img src="/img/wiki/Megamaze.png"  width="278px" height="166px" />
</figure>

**Location:** Megamap<br>
**Type:** Maze

- Is located under the entire Megamap.
- Main entrance is located under an X on the map. You may find Nemo in the process of looking for the entrance to the MegaMaze.


### X Puzzle (aka Finding Nemo)

<figure>
  <img src="/img/wiki/X.png"  width="278px" height="166px" />
  <figcaption><i>No this isn't the correct 'X'</i></figcaption>
</figure>

**Location:** Megamap<br>
**Type:** Puzzle

- Scattered all over the megamap are several X's. One of them hides Nemo.


### The Quest for the Holy Grail

**Location:** Megamap<br>
**Type:** Puzzle

- The Holy Grail [unique]({{< ref "/wiki/gameplay/activities/uniques" >}}) has been in the map for years, though it is no longer in its original location as of September 2018.
- The quest is started by obtaining The Quest for the Holy Grail unique which contains the clue for finding the grail.
- The Holy Grail will not even appear in the map until The Quest for the Holy Grail unique has been obtained. It will remain in the map only until the server resets at which point you too will also have to restart.


## Inactive Puzzles

### Invisible 10-Layer Maze

<figure>
  <img src="/img/wiki/Invismaze.png"  width="278px" height="166px" />
</figure>

**Location:** Olympus (Retired)<br>
**Type:** Maze

- Located somewhere underground.
- There is only one entrance to the maze.
- The maze is entirely made of invisible barriers except for the lower floors.
