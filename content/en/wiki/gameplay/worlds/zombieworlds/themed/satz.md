---
title: "SatZ"
linkTitle: "SatZ"
type: docs
description: >
  A space themed map on a space station
---

L2D's own space station. Beacons provide jump boost to add to the space effect. 

{{< cardpane >}}
{{< card header="**SatZ**">}}

<center>{{< figure src="/img/wiki/L2d_satz.png" width="308px" height="334px" >}}</center>

**First Release:** November 14, 2014 

**First Retirement:** January 6, 2018

**Second Release:** May 22, 2019

**Size:** 243x191

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- Bybloss
- JohnBPKC

{{< /card >}}
{{< /cardpane >}}

SatZ is L2D's space themed map. The map took roughly two months to build.

SatZ provides a permanent jump boost through the use of un-griefable beacons. To protect the beacon beam, the entire central core of the map is also protected.

The map was originally released as the new PvP map, replacing the Islands map; however players did not enjoy the map with PvP so [Islands]({{< ref "/wiki/gameplay/worlds/zombieworlds/themed/islands" >}}) returned as the primary PvP map.

## Trivia

- Despite the main portion of the map not being StarWars related, there are other StarWars ships and machines around the map, such as the Millennium Falcon.
