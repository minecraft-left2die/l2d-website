---
title: "Islands"
linkTitle: "Islands"
type: docs
description: >
  Map of multiple island regions
---

Previously L2D's PvP map, Islands is now part of the themed rotation. Islands is L2D's oldest map still in use. 

{{< cardpane >}}
{{< card header="**Islands**">}}

<center>{{< figure src="/img/wiki/L2d_islands.png" width="308px" height="334px" >}}</center>

**First Release:** December 8, 2013

**First Retirement:** April 26, 2014

**Second Release:** May 11, 2014

**Size:** 400x400

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- Bybloss

{{< /card >}}
{{< /cardpane >}}

The Islands map is the oldest, still in use map on Minecraft Left2Die. It was originally created for the original L2D server created by Bybloss, however, that server was shut down before the map could ever go live. The map had one day where it was previewed by players before it was taken down and not seen again until it was resurrected on the new L2D.

Bybloss is the only known builder for the map, though there were others. 

## Trivia

- When it eventually released, it was incorporated into the [Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) off in the south west corner. Spawn was relocated to this map. It was not a PvP map in the initial release.
- The most recent copy of the map that Bybloss still had was an incomplete version. Before it was released, the map had to be cleaned up of notes left by builders, incomplete builds, and chests and spawners had to be added.
- The original spawn point was the helicopter on the dock. The two flying helicopters were later added with spawn being in one and the tutorial in the other.
- Several of the landmarks in the map were built to represent the clan bases of the top clans at the time the map was made.
- The map was made the primary PvP map when L2D moved to Minecats.
- The spawn area has changed several times to try and thwart spawn camping and players hanging around in spawn and never leaving.
- The map was cleaned up and improved in March 2014.
