---
title: "Aquashock"
linkTitle: "Aquashock"
type: docs
description: >
  Water filled world based on Bioshock
---

Based on Bioshock, this map is completely submerged under water.

{{< cardpane >}}
{{< card header="**Aquashock**">}}

<center>{{< figure src="/img/wiki/Aquashock.png" width="308px" height="334px" >}}</center>

**Release:** May 31, 2014

**Size:** 255x210

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- Bybloss
- JohnBPKC

{{< /card >}}
{{< /cardpane >}}

Aquashock was the first brand new map released after L2D moved to Minecats. It took about a month for the map to be completed. The map is based on the game BioShock.

Aquashock was originally released as a map on its own, but is now part of the themed map rotation. 

## Trivia

- Aquashock was built when the buildmaps were still on the same server as L2D. As such, to avoid crashing L2D when it came time to fill the map with water, Bybloss was on early in the morning, around 2am, to fill it all in.
- The map contains the same mall as is found in the [City]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap/city" >}}) map, only modified for Aquashock and turned into a Casino.
    
    
