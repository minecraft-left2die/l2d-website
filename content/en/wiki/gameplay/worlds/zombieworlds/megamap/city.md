---
title: "City"
linkTitle: "City"
type: docs
description: >
  The Eastern spawn region of Megamap
---

A player favourite. Explore the skyscrapers, but don't get knocked off by a zombie.

{{< cardpane >}}
{{< card header="**City**">}}

<center>{{< figure src="/img/wiki/L2d_city.png" width="308px" height="334px" >}}</center>

**First Release:** June 9, 2013

**First Retirement:** July 29, 2013

**Second Release:** April 26, 2014

**Size:** 400x460

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- zenith4183
- toboein
- OhPistolPete
- PuarZ
- NetCaptive
- mctiger300
- fvqu
- M4gicman
- Sabbing

**Schematics Creators:**

- Keralis

{{< /card >}}
{{< /cardpane >}}

The City map was the first map in the resurrection of the Minecraft Left2Die server. The map was created over the course of a single weekend only taking about two and a half days to complete.

Several of the sky scraper and larger builds were schematics taken from various sources online. No one noted the sources, so the original creators for the majority of the schematics are lost. The exception is the City mall, hospital, and McDonalds which were all created by popular Youtube Minecraft builder, [Keralis](https://www.youtube.com/user/Keralis). Even so, most of the schematics were of skeleton builds and the interiors had to be completed by the L2D build team.

Builds that were not schematics include, but are not limited to:

- The library
- The water tower
- The firehouse
- The Publix (formerly Kruger)
- The Train Station
- The art museum
- Some of the skyscrapers
- Everything located underground
- Builder's Row
- All roadways


## Trivia

- zenith4183 was the only one involved in the creation of the City map that had ever played the original Left2Die server created by Bybloss. As such, he acted as a form on consultant on how the maps and loot should be laid out.
- The original plan was to build a perfect city and then destroy parts of it to make it look abandoned. However, most of the build team did not want to destroy their work, so very few of the buildings were made to look worn down.
- The original spawn was intended to be in the Train Station, however the building was never finished. In fact, the welcome signs for new players continued to stay there for over a year. Spawn was then set in the City mall as several members of the build team did not want to see players destroy the building. Spawn was eventually moved the the helicopter on the hospital to stick with the helicopter transport theme from the original L2D.
- The previously mentioned Train Station is the building across from the library next to the Police Station. There is no train or even train tracks.
- Builder's Row was originally protected as players kept defacing the statues in inappropriate manners.
- Originally, the City had only one unique: Tobo's Magic Stick. The popularity of the item led to more being added.
- The City map has always been the most popular map. When it was replaced with the Town map in July 2013, so many players complained that zenith4183 decided to create the [Megamap]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap" >}}) in response which includes both the City and Town maps.
- The City map was the opening map on L2D when it relocated to Minecats. The other maps were added in shortly after.
- The map received some major cleanup and improvements in March 2015.
