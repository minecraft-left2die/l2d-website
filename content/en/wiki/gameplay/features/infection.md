---
title: "Infection"
linkTitle: "Infection"
type: docs
description: >
  The zombie plague and the cure
---

Zombies carry the zombie plague, aka the infection. The infection spreads through contact with zombies and infected players and comes with many nasty side effects. If a player takes too long to cure themselves, they too will become a zombie until death.

[Plotworld]({{< ref "/wiki/gameplay/worlds/plotworld" >}}) is quarantined, so infected players will be unable to return to plotworld until cured (or death).

## Getting Infected

Infection can be caused by any of the following:

- Hit by a zombie
- Hit by a zombie pigman
- Hit by an infected player
- Eating rotten flesh

There is a 15% chance of becoming infected via any of the above methods. When bitten, an infection level between 1-10 can be passed onto the affected player. A notice will be displayed in chat when you become infection along with a bar indicating your infection level.

[Ranking]({{< ref "/wiki/gameplay/features/ranks" >}}) up will result in a decreased chance of infection.

## Infection Progression

Infection consists of five stages. Every minute, the player's infection level increases by one. Once infected, you will not be able to return to plotworld until cured, so come prepared!

### Stage 1

- Infection Level: 1-3
- Effects: Hunger

### Stage 2

- Infection Level: 4-6
- Effects: Hunger, Wither

### Stage 3

- Infection Level: 7-9
- Effects: Hunger, Wither, Confusion

### Stage 4

- Infection Level: 10
- Effects: Hunger, Wither, Blindness

### Stage 5

- Infection Level: 11-12
- Effects: Hunger, Confusion, Slowness
- Zombification

### Stage 6

- Infection Level: 13
- Death

## Zombification

Upon reaching Stage 5 of the infection, the player will transform into a zombie. While a zombie, interaction in the world is more limited. Zombies also will not attack zombified players unless provoked. Zombified players are unable to cure themselves and must be cured by another player within two minutes or death will result.

Zombified players can attack regular players and vice versa. Zombie players are weaker, slower, cannot eat, open chests, or drop items.

## The Cure

Melon is the only cure for the infection (but only before zombification). Each melon slice will reduce the infection level by one. As such, this will send the infection stages in reverse until fully cured. Milk will remove the effects of the infection, but only temporarily and will not fully cure the player.

Zombified players are cured via glistening melon. The zombified player must have full health to be cured. The player administering the cure will likely need bandages to get them up to maximum health. To cure the zombified player, simply right click them with the glistening melon once they are at maximum health.

