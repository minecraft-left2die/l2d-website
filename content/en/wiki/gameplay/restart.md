---
title: "Server Restart"
linkTitle: "Server Restart"
type: docs
weight: 3
description: >
  Server restarts reset the zombie world.
---

Minecraft L2D restarts every four hours. The restarts are when the [zombie maps]({{< ref "/wiki/gameplay/worlds/zombieworlds" >}}) get reset to restore loot and the buildings. [Plotworld]({{< ref "/wiki/gameplay/worlds/plotworld" >}}) is not effected by the restarts, as such you should build and store loot in Plotworld and not in the zombie world.

You can view how far away the next restart is with the command `/wr` (short for `/whensrestart`). 
