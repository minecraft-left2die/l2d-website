---
title: "Rules"
linkTitle: "Rules"
type: docs
weight: 1
description: >
  The server rules.  Knowing the rules is the player's responsibility.  "I didn't know" is not a valid excuse!
---

## General Rules

{{% alert title="Warning" color="warning" %}}
All hacking, illegal mod, or other forms of cheating related bans are permanent!  There are no appeals.

All other bans will follow a three strikes rule.  The third ban is permanent.
{{% /alert %}}

1. No inappropriate skins, usernames, or Optifine capes.  No inappropriately named items.
2. Do not impersonate staff or other players.  This includes similar usernames and pretending to be staff in chat.
3. No inappropriate builds including, but not limited to: genitalia or other inappropriate adult imagery, NAZI symbols, curse words.
4. Do not use any tools, mods, or exploits to give you an unfair advantage (fly, speed, xray, etc).  If in doubt, ask staff.
	- In general, if it's not listed on the [allowed mods]({{< ref "/wiki/general/client-mods" >}}) page, it is not allowed.
	- An "unfair advantage" is anything that would give you any form of upper hand over a player using a completely un-modded client and nothing more.
	- Watching another player's livestream, YouTube videos, etc to get the location of the best items, uniques, puzzle solutions, etc is an unfair advantage and is bannable.
	- **Bans for breaking this rule are almost always permanent!**
5. Do not exploit glitches or obvious problems with the game whether it be a Minecraft glitch, L2D glitch, map exploit, etc.
	- Report glitches privately to staff.  Exploiting a glitch and not reporting it may result in a ban.
6. Do not intentionally attempt to lag or break the server.  If it is believed to be unintentional, you will receive a warning.
7. No illegal PvP (spleefing (breaking blocks under someone), potions, lava, dropping people with home invites, tnt, etc).
	- Reporting players must have some form of good proof of illegal PvP for staff to act in the event they can not witness it in person. Otherwise, the offending player will just be put on a watch list.
	- The following are **not** illegal PvP and are permitted:
		- If a player runs into lit TNT that was not purposely placed near them and dies
		- If a player breaks a wall and lets zombies in causing another player to be killed
	- Punishment is up to the staff member. Typically punishments are (but not limited to):
		- A warning so long as all items are returned (if applicable)
		- Killing the offending player either destroying the inventory or not.
		- Temporary or permanent ban.
8. Do not build bases, walls, or other structures that will hinder other players' ability from leaving the spawn areas. Keep in mind that the closer you build to spawn, the more likely you will be griefed. Griefing is allowed here!
	- If they have to climb over a wall or look for a door....it's in the way.

### Allowed Activities

The following are actions which are allowed that players are not always aware or cautious of. In general, if you can break it, it's yours. If you can pick it up, it's yours.

- Griefing player builds in the zombie maps.
- Griefing player plots that you have access to.
- Scamming.
- Stealing.
- Breaking blocks that may indirectly lead to a player's death is not considered illegal PvP. For example, breaking the walls of a building and letting zombies swarm in which results in a player being killed is not illegal PvP.


## Chat Rules

Breaking chat rules can result in a mute with or without warning.  Some offenses are monitored and muted automatically. Unless noted otherwise, all chat rules apply to all chat channels, clan chat, direct messages, usernames, item names, signs, books, Discord, etc.

{{% alert title="Warning" color="warning" %}}
Chat rules follow a three strikes rule.  The third non-automated mute is permanent.  Automated mutes may become permanent at staff discretion.
{{% /alert %}}

1. Be respectful of others (including staff!).  No harassing, bullying, or insulting other players.  No sharing the personal information of others.
	- We don't know who you know or your personal relationships with them.  What may be normal behaviour between you and your friends may be perceived as harassment to staff.
	- Personal information of other players includes, but is not limited to:
		- real name
		- age
		- address or general location
		- phone number
		- IP Address
	 - You share information about yourself solely at your own risk.  Assume all chat is public including direct messages.
2. No advertising.
	- Do not talk about other servers or give out server IPs in chat.  This includes server networks that L2D was once part of.
	- No posting Discord invite links.  Share the `/discord` command to let other players know about L2D's Discord server.
	- Do not advertise other websites, products, etc.
	- You may share links promoting your YouTube videos, Twitch streams, or other content so long as they pertain to L2D and that you do not spam or excessively post these links.
3. No spamming.
	- This includes, but is not limited to:
		- single character spam (like counting down)
		- repeated use of all caps (a line or two is fine)
		- random garbage characters
		- breaking up a sentence into several lines
		- non-standard unicode characters (ie anything not seen on your keyboard)
4. No swearing, inappropriate language/inuendo, racism, or posting of images/videos containing or suggesting such.  Keep chat PG.  Do not bypass the chat filters!  No short links or malicious links.
	- Several words are filtered, but not all.  Keep it clean.
	- Yes, "lag" is filtered.  Do not bypass it.
5. No controversial or sensitive topics except in direct messages (with consent of the receiving party).  Even then, discussion must remain civil and respectful to others and their opinions.
	- Including, but not limited to:
		- politics
		- religion
		- sexuality
		- conspiracy theories
6. No talk of topics such as suicide, depression, self harm, etc.  We are not psychiatrists and discussion of these topics here could result in more harm than good.
7. No mini-modding/backseat moderating.
	- Helping out in chat is okay, acting like you are staff and threatening to get players muted/banned is not okay.
	- Do not accuse players of hacking/cheating.  Make a `/report` and leave it at that.  Do not provoke suspected hackers or accuse them in chat.  It makes it harder for us to catch them if they think staff may be checking in on them.
8. Do not ask staff for items/animals or for the locations of items in the map.
9. Do not ask for the locations of unique or OP items in chat.  Do not ask for answers to puzzles in chat.  Do not openly discuss either of these things in global chat.
	- Working together with other players is okay, but stick to local chat or direct messages.
	- Straight out giving locations to unique/OP items or puzzle answers is not allowed.  It must be a collaborative effort (this includes trading info).
10. Staff have final say on all matters.  Do not argue with them!  If you believe a staff member is wrong in their judgment, take it up with a higher up staff.  Chat Mods --> Mods --> Admins

## Discord Rules

1. All of the server chat rules apply to the Discord server.
2. No copy-pastas, large blobs of random text, asci art, chain messages, or other spammy content.
3. The swear filters on Discord are not as thorough as on the Minecraft server, this does not mean you can use that language and your posts will likely be deleted by staff and you may be muted.
4. No inappropriate Discord avatars or statuses.
5. Do not @staff members for mundane things or anything that any other member in the Discord could reasonably be expected to know the answer to.

## Ban Appeals

We do not accept ban appeals.  Bans that do not have an expiration date set on them are permanent. 

