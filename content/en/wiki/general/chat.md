---
title: "Chat"
linkTitle: "Chat"
type: docs
description: >
  How chat is organized on the server.
---

L2D has several chat channels to help keep chat organized. [Chat Mods]({{< ref "/wiki/general/staff#chat-moderators" >}}) are responsible for moderating and enforcing chat rules. If they tell you not to do something, listen to them. They can mute with or without warning. As such, players should read and follow the [rules]({{< ref "/wiki/general/rules" >}}) to avoid being muted.

There are also auto-mutes which trigger for certain chat offenses. Automatic mutes are very strict and mute all forms of communication (chat, private messages, signs, books, renaming items, etc). The more auto-mutes a player receives, the longer the mute will last.

{{% alert title="Warning" color="warning" %}}
Chat Mods can mute without warning for breaking chat rules.  Some mutes are implemented automatically.
{{% /alert %}}

## Channels

Please keep chat in the relevant channels. Mistakes happen where players accidentally use the wrong channel and we're fine with that, but stop misusing a channel when asked or you may be hit with a mute.  Quick chat commands can be used to talk in a channel without having to switch your primary channel.

| Channel | Description                                                                                                             | Join Command | Quick Chat Command            |
| ------- | ----------------------------------------------------------------------------------------------------------------------- |:------------:|:-----------------------------:|
| local   | Use this if you can see the person you are talking to.                                                                  | `/ch l`      | `/ch l <msg>`<br>`/l <msg>`   |
| global  | Intended for general talk among all players. Don't use this if you are talking to a particular person whom you can see. | `/ch g`      | `/ch g <msg>`<br>`/g <msg>`   |

## Other Commands

| Command                | Description                                                                   |
|:----------------------:| ----------------------------------------------------------------------------- |
| `/chlist`              | Lists all of the above channels.                                              |
| `/chlookup`            | Displays first and last login times, and which channels you are listening to. |
| `/lev <channel>`       | Leaves a channel so you no longer see messages in that channel.               |
| `/tell <player> <msg>` | Send a player a private message.                                              |
| `/chignore <player>`   | Ignore tells from a player.                                                   |
