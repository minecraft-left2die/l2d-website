---
title: "Server Policies"
linkTitle: "Server Policies"
type: docs
weight: 4
description: >
  Policies describing how the most common situations on the server will be handled.
---

This is not a complete page and will be added to as things come to mind. The policies on this page are subject to change at any time, for any reason, with or without warning or notice. 

## Bans

* Staff require no more proof other than seeing it with their own eyes to implement a ban. Some bans may be implemented solely on evidence collected by our hacking detection system though we will normally try to get an actual visual.
* Hacking related or serious/repeated offence bans are permanent with no appeal. Other bans will have a time set to them on when they will automatically be lifted and will not be lifted earlier than that time.
* Players are solely responsible for the security of their account. "My account was hacked" or My brother/sister/friend did it" is not a valid appeal.

## Chat

* Assume everything you say in chat is being read by an Admin or other staff member. This includes, but is not limited to, regular chat, `/tell`, and `/msg`. Other forms of communication may also be monitored such as signs and books. There should be no expectation of complete privacy.
	* Due to the unencrypted and public nature of communication on a Minecraft server, the possibility of unknown vulnerabilities, the possibility of volunteer staff members gone rogue, among other things, there is no guarantee that chat messages sent on the server, private or otherwise, will not be leaked or revealed elsewhere. It is purely the responsibility of the player to not reveal any personal information via the in-game chat (public or private messages) that they would not want to risk becoming public.
* Private messages are typically not looked at unless needed to investigate another issue.
* Abuse of `/me` via spamming or using it as if it were a channel may result in the loss of permissions.
* See the mute section below on muting policy.


## Items

* Items lost to glitches will not be replaced.
* Whether items received via accidental glitches (hacking is not a glitch) will be taken away or not will be evaluated on a case-by-case basis. Intentionally using a glitch to your advantage and not reporting it may result in a ban or in any items achieved via the exploit being revoked.
* Items lost to hackers will not be replaced.
* Items lost to illegal PvP will not be returned unless the PvP-er is caught in the act and even then, this is not guaranteed.
* Players are not entitled to any freebies under any circumstance outside of the kits.
* Named items are subject to the chat rules. If you plan to use a weapon in PvP, do not use inappropriate names.
	* Staff will ask that the weapon be renamed first. Failure to comply may result in loss of all inventory and/or ban from the server.
* On rare occasions, some server-side lag inducing items may be wiped from the server when there are noticeable issues occurring. Items known for causing server side-lag include, but are not limited to: hoppers, item frames, armour stands, redstone components.


## Money

* Money lost to glitches will not be replaced.
* Scamming is allowed, so money lost to scams will not be replaced.
* Money is subject to being wiped. This is normally done with warning when possible and is only done when it is completely unavoidable due to plugin update or glitch.


## Mutes

* Contrary to popular belief, L2D (and other online communities) are not subject to "freedom of speech". Players can be muted or banned for the things they say.
* Chat Mods do not need to give warnings (as stated in the rules themselves).
* Unmutes are automatic. Bothering staff for an unmute may result in an increased mute time at the staff member's discretion.
* Some mutes are automatic.  These mutes are very strict and result in all forms of communication being blocked (chat, `/msg`, signs, books, renaming items, etc).
	* The more times a player is muted via the automatic muting system, the longer the mute time becomes.


## Player Names

* Changing your name to resemble the name of a staff member or another player is subject to a ban until the name is changed.
* Players with inappropriate names are subject to mutes or bans until the name is changed. This is normally not acted upon unless it is particularly bad (ie F-bomb) or until another player complains.


## Plots
### Animals/Mobs

* The animals/mobs on plots may be reset at any time without compensation to reduce server lag.
	* Typically cats, dogs, horses, and rabbits will be protected, but there is no guarantee.
* If a player has an excessive number of animals/mobs on their plot, all animals/mobs will be removed from the plot by staff.
* Animals/mobs killed by other players will not be replaced.

### Claiming

* Players may claim any unclaimed plot they like within the boundaries of plotworld.
* Plots are first come, first served.
* Players may not claim any plot that is currently owned and unexpired.
	* Plots will not be transferred to another player, even to alt accounts belonging to the same person.
* Staff will not change player permissions of a plot.
* If a player sets their plot for sale either intentionally or by mistake, staff will not reclaim ownership if an unintended buyer buys the plot.
* Staff will not restore ownership of a plot which the original owner disposed of should another player take ownership.

### Combining

* Staff will not merge plots for you.
* All plot commands will act on a merged plot as one. Staff will not replace anything lost as the result of a command that clears any or all of the merged plot being executed.
* Staff will not replace anything lost as the result of a plot co-owner (result of merging with another player) performing a command that clears any or all of the merged plot.


### Expiration

* After three months of no login or block activity, the plot will automatically be deleted.
* There will be no compensation for a plot lost to expiration.


### Grief

* Grief of no sort will be repaired. This includes items taken from chests. It is the player's responsibility to only add trustworthy members to their plot.


### Protecting

* ZombieSlayer rank and higher may protect and un-protect their plots at will via /p protect.
* At staff discretion, plots of long time players and plots with exceptional builds may be protected for free. There is absolutely no guarantee to this and this protection may be revoked at anytime.
* Staff will not move protected plots without consent from the owner unless space is needed around the spawn area or if PlotWorld is being contracted in size.
* Underdeveloped plots that are protected may have that protection revoked and the plot deleted if the owner becomes inactive for several months.
	* "Underdeveloped" is considered any plot that is completely blank or that contains nothing of value.
