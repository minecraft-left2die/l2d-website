---
title: "Client Mods"
linkTitle: "Client Mods"
weight: 2
type: docs
description: >
  A list of allowed client mods.
---

The general rule of thumb with Minecraft mods is if it gives you an unfair advantage, you cannot use it. Below are some mods that are explicitly allowed and denied on Left2Die. This is not a complete list. If you have any doubts about a mod, ask a [staff]({{< ref "/wiki/general/staff" >}}) member.

{{% alert title="Warning" color="warning" %}}
Use of an illegal mod, whether listed here or not, will result in a permanent ban!  There are no appeals.
{{% /alert %}}

## Explicitly Allowed Mods

* Optifine/Sodium/Lithium/Phosphor
* Chat mods such as TabbyChat
* Minimaps with overhead view only (no cave finders or other xray features) and no player radar or chest finders
* Health indicator mods
* Potion effect indicator mods
* Armour status mods
* BetterPvP Fair Play version
* Shaders

## Explicitly Illegal Mods

All bans due to illegal mods are permanent with no option to appeal whether the mod is listed here or not.  If in doubt, ask staff!

* X-Ray (including texture packs!)
* Chest finders
* Fly
* Kill aura or auto-attack
* Auto equip armour, weapons, tools, etc mods
* Auto loot mods
* Minimaps with cave view, chest finders, xray features, or player radar
* World downloaders
* Recording mods that give spectator or similar modes
* Enhanced PvP mods such as BetterPvP normal version
* Macro mods

