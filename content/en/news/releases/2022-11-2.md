---
title: "Release 2022.11.2"
linkTitle: "2022.11.2"
date: 2022-11-13
type: blog
robotsdisallow: true
exclude_search: true
---

## Changelog

- Netherite Casino added to Nether map.
  - ~2% chance of nether debris, 98% chance death
- Quality of life improvements:
  - Farmland can be watered with a water block placed underneath.
  - Using a hoe to harvest crops will auto-plant new seeds.
  - Holding shift while breaking stacked slabs will break the bottom slab.
  - Right click a sign while holding a sign to edit it (Plotworld only).
- Consolidating chat channels for smaller community:
  - Discord channels for #help, #donators, and #mature removed.
  - Mature chat channel removed in game.

