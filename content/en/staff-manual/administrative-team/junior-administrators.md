---
title: "Junior Administrators"
linkTitle: "Junior Administrators"
type: docs
weight: 2
robotsdisallow: true
exclude_search: true
---

## Responsibilities

* Same as those of the lower moderation team in their absence.
* Act as a senior member to the moderation team when they have questions, need advice, or need a second opinion.  This also includes managing the moderation team in the events of absence, increasing the staff size, and disciplining and/or demoting staff.
* Handle any issues in-game that the lower moderation team are not capable of such as dealing with in-game glitches or dealing with server lag issues (mob butchering).
* Handle any player disputes that have escalated beyond the lower moderation team.
* Create in-game events such as mob spawning parties.  Do not do drop parties.
* Have access to Admin (a) chat.

## Abilities

* Can enter and build in any protected regions.
* Will receive notifications when a player enters an X-Ray trap region.
* Can see other vanished staff.
* Can see all messages sent in clan and ally chats.

## Guidelines

### General

* Junior Administrators should know the ins and outs of the server policies as detailed on the wiki.
* Junior Administrators should handle any situation that arises on the server that is not covered on the policies page to the best of their abilities and judgement.  If in doubt, discuss with a Head Administrator.  If the situation is felt to be significant enough, it should be discussed with the Head Administrator to determine what the best official policy going forward should be.

### Creative Mode

* Don’t give out items unless you are doing it as part of a contest where any player can participate.  Items given out as prizes should not be “OP” in nature (ie no enchant levels beyond the Minecraft maximum).  Freebies should not be easy for players to get, so no drop parties.
* Use the vanish command to hide yourself when flying unless you are hovering over an area for an event.  It does not look good to new players (or old)  to see someone flying around.  They will not necessarily know that you are staff.
* You may create special items for yourself for event purposes (ie prot IV armour), but must destroy those items upon completion of the event.  Keep the gear you use for personal play time separate from gear you take from the creative inventory.

## Commands

Includes all Chat Moderator and Senior Moderator commands. 

Jr. Admins also have full perms to Command Book, Crazy Auctions, and PlotSquared and most permissions of WorldEdit (limitations to protect from server crashes):

* [https://www.enginehub.org/commandbook/](https://www.enginehub.org/commandbook/)
* [https://www.enginehub.org/worldedit/](https://www.enginehub.org/worldedit/)
* [https://www.spigotmc.org/resources/plotsquared.1177/](https://www.spigotmc.org/resources/plotsquared.1177/)
* [https://www.spigotmc.org/resources/crazy-auctions.25219/](https://www.spigotmc.org/resources/crazy-auctions.25219/)

### WorldGuard

| Command | Description |
| ------- | ----------- |
| /god [player] | Puts yourself or the specified player into god mod.  Should only be used on players when protecting them from something you’re doing/testing.  Always remove with /ungod. |
| /heal <player\> | Heals and fills the hunger bar of the specified player. |
| /locate <player\> | Points your compass in the direction of the specified player. |
| /slay <player\> | Kills the specified player.  Not to be confused with /kill which kills the player running the command. |
| /ungod [player] | Removes god mod from yourself or the specified player. |

### Other Plugins

| Command | Description |
| ------- | ----------- |
| /lagg area <radius\> | Despawns mobs, dropped items, and other entities within the specified radius. |
| /money balance <name\> | Shows the balance of the specified player’s bank account. |
| /openinv <player\> | Opens the inventory of the specified player. |
