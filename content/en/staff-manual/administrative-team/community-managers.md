---
title: "Community Manager"
linkTitle: "Community Manager"
type: docs
weight: 3
robotsdisallow: true
exclude_search: true
---

## Responsibilities

* Manages all of the server’s social media accounts including Twitter, Instagram, Youtube, and Steam.
* Creates and coordinates community server events such as voting events, plot contests, etc.


## Guidelines

* Because people don’t always follow all of the social media accounts, try and post to all accounts where applicable.
