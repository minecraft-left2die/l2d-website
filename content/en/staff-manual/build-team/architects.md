---
title: "Architects"
linkTitle: "Architects"
type: docs
weight: 1
robotsdisallow: true
exclude_search: true
---

## Responsibilities

* Responsible for coming up with map ideas and creating the general idea for its setup.
* In charge of the build team.
	* Makes the decision on what builders will work on what maps and which builders receive which responsibilities.
	* Regularly seeks out new potential builders.

## Guidelines

* Same guidelines as for all Builder tiers.
* Architects are largely given free reign to decide how a map gets built.
* All new maps should be run past the head admin prior to starting.


## Reduced Restrictions

Architects have the following reduced restrictions:

* Can enter and edit any map, world, or region.
* Can increase their WorldEdit limit.
* Can access all chests.

## Commands

Architects have access to all commands of all of the builder tiers in addition to the following:

### General

| Command | Description |
| ------- | ----------- |
| /time [world] <day|night\>  | Set the time to day or night.  Optionally, specify the world. |

### Permissions

| Command | Description |
| ------- | ----------- |
| /whitelist add <player\> | Whitelists a player to the build server. |
| /whitelist remove <player\> | Un-whitelists a player. |
| /whitelist list | Lists all currently whitelisted players. |
| /lp user <user\> parent add <group\> [time] | Adds the specified group to the specified user.  See the list of groups below.  You can optionally specify a time in seconds to auto-remove the group after a specified period of time. |
| /lp user <user\> parent remove <group\> | Removes the specified group from the specified user.  See the list of groups below. |
| /lp user <user\> parent info | Lists all groups the specified user is in. |

### Worldedit

| Command | Description |
| ------- | ----------- |
| //limit # | Changes your WorldEdit block limit.  The current maximum value you can set is 100000. |
| /fixwater <radius\> | Levels pools of water within the given radius. |
| /fixlava <radius\> | Levels pools of lava within the given radius, |
| //forest [type] <density\> | Generates a forest of given type and density. |

## Worlds

| World |  Description |
| ----- | ------------ |
| world | Themed maps and PvP (Islands).  Also contains the old versions of City and Town and a few other special occasion areas like FTD. |
| world2 | Olympus |
| world5 | The current Megamap |
| buildworld | The sandbox world |

## Permissions

| Permission Group | Permissions Granted | Description |
| ---------------- | ------------------- | ----------- |
| builder | | Provides all of the necessary perms that most builders will need.  All builders must be in this group to be able to do anything. |
| weperms | worldedit.selection.shift<br>worldedit.selection.expand<br>worldedit.navigation.up<br>worldedit.region.set<br>worldedit.selection.inset<br>worldedit.selection.outset<br>worldedit.selection.contract<br>worldedit.wand<br>worldedit.selection.pos<br>worldedit.region.move<br>worldedit.region.stack<br>worldedit.region.replace<br>worldedit.region.faces<br>worldedit.region.walls<br>worldedit.history.*<br>worldedit.generation.sphere<br>worldedit.generation.cylinder<br>worldedit.clipboard.flip<br>worldedit.clipboard.rotate<br>worldedit.clipboard.cut<br>worldedit.clipboard.paste<br>worldedit.clipboard.copy | Gives the basic WorldEdit perms needed by most users using WorldEdit.  Should only be given to players that know how to use WorldEdit.  The build server should not be used as a training ground except in the sandbox.<br><br>This perm can be limited to a specific world via:<br>/lp user <user\> parent add weperms world=<world-name\> |
| weperms2 | worldedit.brush.options.mask<br>worldedit.brush.sphere<br>worldedit.brush.smooth<br>worldedit.brush.options.material | WorldEdit brush perms.  As these can be very irrevocably destructive, this perm should only be given to trusted builders that know how to use it already.<br><br>This perm can be limited to a specific world via:<br>/lp user <user\> parent add weperms world=<world-name\> |
| spawnbuild<br>citybuild<br>townbuild<br>aquashockbuild<br>tinybuild<br>netherbuild<br>satzbuild<br>titanicbuild<br>megamapbuild<br>olympusbuild | group.<worldguard-group-name\><br>mywarp.warp.world.<world-name\> |Gives access to build in a certain map and also access to enter that world. |
| allmapsentryworld1 | group.aquashocknobuild<br>group.nethernobuild<br>group.satznobuild<br>group.tinynobuild<br>group.titanicnobuild<br>group.islandsnobuild<br>mywarp.warp.world.world | Gives access to all of the worlds for the main maps (mega, pvp, themed).  This does not give build access.<br><br>The individual group perms can also be given to allow a player read-only access into a map that do not have build access in. |
| world1access<br>world2access<br>world5access | mywarp.warp.world.<world-name\> | Gives a player access to a world, but no permissions to enter or edit a map.  Mostly used in combination with a nobuild group perm (see above). |
| chestaccess | Gives access to chests, trapped chests, furnaces, minechat chests, minecart furnaces, droppers, minecart hoppers, anvils, hoppers, dispensers, item frames, armour stands, shulker boxes | Should be delegated on a world by world basis.<br><br>/lp user <name\> parent add chestaccess world=<world-name\> |
