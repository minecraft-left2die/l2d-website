---
title: "Senior Moderators"
linkTitle: "Senior Moderators"
type: docs
weight: 1
robotsdisallow: true
exclude_search: true
---

## Responsibilities

* Same as Chat Moderator and Moderator.
* Act as a senior member to the lower ranked moderation team when they have questions, need advice, or need a second opinion.
* Help make the server more fun by starting server events when time allows.


## Guidelines

* Same as for Moderators.


## Abilities

* All abilities of the lower ranked moderation team
* Can bypass build permissions in any protected region of the zombie worlds.
* Can see vanished mods.

## Commands

Includes all Chat Moderator and Moderator commands.

### WorldEdit

It is recommended that you use the mod WECUI for all WorldEdit operations.  WorldEdit operations on L2D use a queueing operation to prevent server lag.  These commands are only available in plotworld.

| Command | Description |
| ------- | ----------- |
| //pos1<br>//pos2 | Set postions 1 and 2 of a region.  Can also be done via the WE wand (wood axe). |
| //wand | Obtain a WE wand. |
| //expand [#] <up/down/north/east/south/west\> | Expand the region by the given amount in the direction you are facing or the direction specified. |
| //contract [#] <up/down/north/east/south/west\> | Same as expand only shrinking the region. |
| //outset <-h> [#] | Expands the region the specified amount in all directions.  -h flag will only expand in the horizontal direction. |
| //inset [#] | Same as outset on shrinking the region. |
| //limit [#] | WorldEdit by default limits block operations to 2000.  Use this command to raise it up to 300000. |
| //replace [block1] [block2] | Replaces all of block1 in the region with block2.  Can also specify a comma separated list of blocks for block1.  Block2 can be a comma separated list of blocks denoted with percents.  ie //replace wood 50%stone,50%dirt |
| //stack [#] <up/down/north/east/south/west\> | Takes the selection and duplicates it in the direction you are facing of the specified direction for the number of times specified. |
| //copy | CCopies the selection. |
| //cut | Cuts the selection (thus removing it). |
| //rotate [#] | Rotates the selection by the specified number of degrees. |
| //flip <up/down/north/east/south/west\> | Flips the blocks copied to the clipboard in the direction specified. |
| //paste | Pastes the blocks in the clipboard. |
| //schematic list | Lists all schematics on the server. |
| //schematic load [name] | Loads the schematic of the give name.  Use //paste to paste it. |
| //undo | Reverts the last command.  Due to the WE limiting plugin on L2D, do not run this command a second time until the first has completed. |
| //redo | Redoes the last command after being undone.  Due to the WE limiting plugin on L2D, do not run this command a second time until the first has completed. |
| /p weanywhere<br>/p wea | Allows for WE to be used on any players plot.  **USE WITH EXTREME CAUTION!**  You can easily irrevocably destroy a player’s plot by doing this (lost chest items, lost item frames, etc). |

### Other

| Command | Description |
| ------- | ----------- |
| /slap <-shv\> [player] | Slaps the specified player.<br>-s: silent<br>-h: hard slap<br>-v: very hard slap<br>Don’t slap players that are behaving. |
| /spawnmob <-irbt\> [mob] [amount] <player\> | Spawns the given mob the specified amount of times up to 10 at a time. Optionally specify a player to spawn them at.<br>-i: ignites mob<br>-r: rockets mob<br>-b: baby mob<br>-t: tamed mob |
| /gm [0,1,2,3] | Change your game mode.<br>0: survival<br>1: creative<br>2: adventure<br>3: spectator |
