---
title: "Moderators"
linkTitle: "Moderators"
type: docs
weight: 2
robotsdisallow: true
exclude_search: true
---

## Responsibilities

* Same as Chat Moderators.
* Responsible for banning particularly bad players.
* Handles ban appeals for their bans.
* Respond to player reports within their abilities.

## Abilities

* All previously mentioned moderation staff perks.
* No cooldowns or warmups on warping.
* Bypass plot entry blocking.
* Bypass plot entry/exit notifications to their owners.

## Guidelines

### Bans

* Ban for the following:
	* Advertising
		* Don’t ban for Mineplex, Hypixel, or any other hugely popular server that anyone would likely already know of unless they ignore warnings to stop.
		* If they are telling a specific friend an IP, temp ban them.  If they are telling the entire server, permanent ban them.
		* The advertising rule only applies to other Minecraft servers.  Youtube accounts and similar are allowed as long as it’s not spammed or promoting another server.
	* Hacking
		* Don’t ban unless you are sure.  If in doubt, ask another Mod to check also for a second opinion.
		* PvP has a cooldown that will prevent members of a clan from killing a player killed by another clan member.  Do not ban players for so called “invincibility” hacks in PvP as it is most likely the cooldown.
	* Illegal PvP (temp bans)
		* Only act if you see it.  Most “proof” players provide is mediocre at best and often times it comes down to he-said-she-said
		* If the victim survived, just warn the offender.  If they continue, either kill the offender or give a temp ban.
		* If the victim died, give the offender a chance to return any items.  Otherwise, either kill the offender or give a temp ban.
	* Any large disturbance or threats to the server.
* Don’t specify hack types in the ban message.  “Hacks” is good enough and means you can require them to fess up to the particular hack before you will consider unbanning them.  Sometimes you will find they confess for something you never knew they had as a result.
* We ban accounts, not players.  Don’t ban an account just because the player is banned on another account unless they immediately start an issue up again or have a history of repeatedly causing issues.  The exception to this is players caught X-raying or using any hacked client or mod that allows them to easily find hidden loot chests.  You will be able to see a player’s alts and will be notified if a banned player logs in on an alt.

### Ban Appeals

* We are no longer accepting ban appeals.  All hacking related bans are now permanent.  Non-hacking bans that are not for extreme offenses should typically be done as a temp ban.

### Player Reports

* Try and check player reports when logging into the server.  Respond to any within your ability.
* Try and respond to new player reports as soon as possible.

### Names

* If a clan has an inappropriate name or tag, disband it.  If the player makes another distasteful clan, ban the player from clans.
* If a player uses an inappropriately named item in PvP, ask them to rename the weapon.  If they refuse, give a temp ban.
* If a player has an inappropriate name, use your best judgement to either mute in all channels or temp ban the player for a month with the ban message instructing them to change their name to something more appropriate.  If they return with the same or another offensive name, ban them permanently.  Only the really offensive names should be banned.  In general, if no one is complaining or commenting on the name, it is probably okay to let them play.


## Commands

Includes all Chat Moderator commands.

### Reports

| Command | Description |
| ------- | ----------- |
| /reports list | Lists player reports. |
| /reports info <#\> | Shows the details of the given report. |
| /reports tp <#\> | Teleports you to the location the report of given ID number was made at. |
| /reports close <#\> <comment\> | Closes the given report. |

### Bans

| Command | Description |
| ------- | ----------- |
| /kick <player\> <message\> | Kicks a player from the server with the given message. |
| /ban <player\> <message\> | Bans a player from the server with the given message. |
| /tempban <player\> <time\> <message\> | Bans a player for the given amount of time. |
| /baninfo <player\><br>/bminfo <player\> | View the ban status, warnings, and alternate accounts for the given player. |
| /unban <player\> | Unbans a player. |
| /warn <player\> <reason\><br>/warn -p <points\> <player\> <reason\> | Warn a player.  This warning will be visible to other staff via /bminfo.  They are also publically visible on our website, so keep messages professional.  Using the /warn command will create a persistent warning.  For a temporary warning, use /tempwarn (see the Chat Moderator command list).  Using the -p flag will allow you to set a warning severity (1-10). |
| /dwarn <player\> | Deletes the most recent warning given to a player. |
| /addnote <player\> <message\> | Adds a note on a player.  This will be visible to other staff via /bminfo or /notes, but will not be visible to the player or on the website.  **You cannot delete these**. |
| /notes <player\> | View the current notes on a player. |

### Vanish

| Command   | Description |
| --------- | ----------- |
| /v        | Vanish/unvanish from all players.  Gives fly.  **DO NOT ABUSE THIS!** |
| /svlogin  | Sends a fake login message. |
| /svlogout | Sends a fake logout message. |


### L2D Plugin

| Command | Description |
| ------- | ----------- |
| /stats <player\> | View stats of another player. |
| /rank <player\> | View rank of another player (displayed as a number starting at 0 for ZombieBait). |

### Teleportation

| Command | Description |
| ------- | ----------- |
| /tp <player\> | Teleports you to a player. |
| /tp <player\> <your-name\> | Teleports a player to you. |
| /jumpto | Teleports you to the block you are looking at.  Can also be done by left clicking a compass. |
| /thru | Teleports you through a wall.  Can also be done by right clicking a compass. |

### Chat

| Command | Description |
| ------- | ----------- |
| /ale <message\> | Displays an alert to the server. |
| /chwho <channel\> | Display all players currently listening to the given chat channel. |
| /bcast <message\> | Sends a message to the `#general` channel in Discord. |

