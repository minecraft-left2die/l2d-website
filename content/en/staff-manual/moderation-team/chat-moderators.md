---
title: "Chat Moderators"
linkTitle: "Chat Moderators"
type: docs
weight: 3
robotsdisallow: true
exclude_search: true
---

## Responsibilities

* Maintain control of the chat channels.
* Mute and unmute players that disobey the chat rules.
* Assist player with questions on playing.
* Chat Moderators have access to Staff (st) chat.

## Non-Responsibilities

* Dealing with hackers.
* Dealing with other bannable offenses other than alerting a moderator.

## Guidelines

### Chat Control

* **There is no rule on caps.**  There is, however, a rule on spam.  A few lines of caps is okay, it is only when the player is saying everything for several lines in caps or spamming the screen in quick succession that it is mutable.
* The “be kind and courteous” rule is very loose.  On a server of death, scamming, stealing, and PvP, there will be feuds and words thrown about.  Use your best judgement on when things have gone too far.
* Players do not need to be given a warning, as stated in the rules.  It is up the the Chat Moderators’ discretion to decide whether to warn or mute first.
* If you do choose to warn a player, try and make sure they will know that it is them you are warning.  Sometimes they will ignore or miss things in chat as it is moving quickly.
* Don’t mute players for bypassing “lag”, but do warn them.  If they continue repeatedly after being warned, then you may mute but use a short duration (10-20 minutes).
* If a player is accusing a player of hacking and a moderator or admin is not online, take any conversation to tells in order to get details and/or to ask them to make a modreq.  Don’t discuss hackusations in the chat channels.
* Chat Moderators have chat colours to make themselves more noticeable to players when needed, such as an announcement or to get an unruly player’s attention.  They are not your personal means to get yourself noticed for anything other than in instances related to the job of Chat Moderators.
* Do not go overkill with mute durations.  A player that raged a bit after dying does not need to be muted for two months.  Unless the offense is severe, start will low durations and increase the duration on repeat offences.
* You cannot moderate a channel if you are not in it.  As such, Chat Moderators are expected to be in all of the chat channels at all times.

### Player Questions

* It is better to tell a player that you do not know the answer than to give false information or ignoring them entirely.
* Sometimes players don’t see the first response or don’t know it was directed at them when chat is moving.  Some players will also ignore players in the same rank as them or ignore non-staff members.  Don’t get harsh on them if they missed the first response, but if they continue to repeatedly ask the same question in chat and ignore every answer, you may need to really single them out or mute them.
* Try and guide players towards finding answers for themselves.  Advise new players to read the tutorial.  Give players /wiki commands to let them read for their answer.  Eventually, most will start to just go to the available resources to get their questions answered without even asking them.
* Just because one player is asking a question does not mean no one else is thinking it.  Respond to questions in chat in the channel they asked it in (even if it’s the wrong channel), not in tells so other players may benefit.
* If they ask for something that only a mod or higher can do, instruct them to contact a mod or admin.
* This is not a beginner server.  If a player is asking how to craft basic blocks, point them to minecraftwiki.net.  Answering something as simple as “How do I make a chest” in chat will lead them to ask every possible beginner question in chat rather than learning basic Minecraft themselves.

### Advertising

* If a player is advertising, mute them in all channels.  If a moderator or admin has not banned them by the time they are muted everywhere, make sure they are aware.  Ping in #staff on discord if no mods or admins are online.
* If a player sounds as if they may give out an IP in chat (“let me get you the IP”), mute them in the channel they are using and send them a tell.  Once they acknowledge that they won’t give out the IP, unmute.
* Warn if they bypass the Mineplex or Hypixel filters and only mute if they continue after the warning.  These servers are popular enough that it is barely advertising.

### Unmutes

* If a player asks to be unmuted, ask these two questions: 1) Why were you muted?, 2) Will you do it again?.  As long as they can satisfactorily answer these two question, unmute the player (unless they were just muted).  If you did not mute the player, it is often best to ask any other staff in the channel if they were the muting staff before dealing with the player yourself.

### Channels

* Mistakes happen.  Don’t harp on a player that says something in the wrong channel every so often.  If they ask a question, answer it in the same channel even if it’s the wrong channel.  After which, you may correct them and guide them to the correct place for future reference.  Don’t tell them to “ask in this channel” before you’ll answer.
* If a topic in a channel veers off topic for that channel and no one else is trying to use that channel for its intended purpose, just let it go for a while and most of the time it will wear down on it’s own.  If it goes on too long or if someone else is trying to use it for its correct topic, politely ask them to move to the proper channel.
* Local should only be muted if they are muted in every other channel already or are misbehaving in the spawn areas.  If you do need to mute in local in a particularly bad case, then also mute global.

## Commands

| Command | Description |
| ------- | ----------- |
| /mute <player\> <channel\> [duration] | Mutes a player in the specified channel for the given duration.  Default duration (if not specified) is one day.<br><br>*Example Durations:*<br>1d<br>4h<br>10m<br>1d-4h<br>1d-4h-10m<br>1d-10m |
| /unmute <player\> <channel\> | Unmutes a player from the specified channel. |
| /muteall <player\> | Mutes a player in all channels. |
| /muteme <player\> [duration] | Similar to channel mutes only this mutes /me. |
| /unmuteme <player\> | Unmutes a player from /me. |
| /chforce <player\> <channel\> | Forces a player into a channel. |
| /chkick <player\> <channel\> | Kicks a player from a channel. |
| /fl <player\> | Forces a player into local chat. |
| /cc <br> /clearchat | Clears chat.  Use only when absolutely needed (advertising, explicit links, inappropriate material, and similar) |
| /vr remind | Displays a reminder message on screen for players to vote. |
| /tempwarn <player\> <duration\> <reason\> | Warns a player.  This warning will be visible to other staff via /bminfo.  They are also publicly visible on our website, so keep messages professional. |
| /bminfo <player\> | Shows the current warnings (if any) for the specified player. |

### Discord Commands

| Command                        | Description                           |
| ------------------------------ | ------------------------------------- |
| /warn @<USER\> <REASON\>       | Warns a user with reason.             |
| /infractions @<USER\>          | Lists warning history against user.   |
| /mute @<USER\>                 | Mutes user permanently until unmuted. |
| /unmute @<USER\>               | Unmutes user.                         |
| /tempmute @<USER\> <DURATION\> | Mutes user for specified duration.<br><br>*Example Durations:*<br>1d<br>4h<br>10m<br>1d4h<br>1d4h10m<br>1d10m |
